use crate::prelude::*;
use legion::systems::CommandBuffer;
use ron::de::from_reader;
use serde::Deserialize;
use std::collections::HashSet;
use std::fs::File;

/// Represents the type of an entity in a template
#[derive(Clone, Debug, Deserialize, PartialEq)]
pub enum EntityType {
    Enemy,
    Item,
}

/// Represents a level entity template
#[derive(Clone, Debug, Deserialize)]
pub struct Template {
    pub entity_type: EntityType,
    pub levels: HashSet<usize>,
    pub frequency: i32,
    pub name: String,
    pub glyph: char,
    pub provides: Option<Vec<(String, i32)>>,
    pub hp: Option<i32>,
    pub base_damage: Option<i32>,
}

impl Template {}

/// Represents a templates file
#[derive(Clone, Debug, Deserialize)]
pub struct Templates {
    pub entities: Vec<Template>,
}

impl Templates {
    /// Loads data from a file and returns a Templates
    pub fn load() -> Self {
        let file = File::open("resources/template.ron").expect("Failed opening file");
        from_reader(file).expect("Unable to load templates")
    }

    // Creates an entity following a template, and adds it to a World via a Command Buffer
    pub fn spawn_entity(&self, pt: &Point, template: &Template, commands: &mut CommandBuffer) {
        // Add essential components
        let entity = commands.push((
            pt.clone(),
            Render {
                color: ColorPair::new(WHITE, BLACK),
                glyph: to_cp437(template.glyph),
            },
            Name(template.name.clone()),
        ));

        // Add specific components based on entity type
        match template.entity_type {
            EntityType::Item => commands.add_component(entity, Item {}),
            EntityType::Enemy => {
                commands.add_component(entity, Enemy {});
                commands.add_component(entity, FieldOfView::new(6));
                commands.add_component(entity, ChasingPlayer {});
                commands.add_component(
                    entity,
                    Health {
                        current: template.hp.unwrap(),
                        max: template.hp.unwrap(),
                    },
                );
            }
        }

        // Add effect components if present
        if let Some(effects) = &template.provides {
            effects
                .iter()
                .for_each(|(effect, n)| match effect.as_str() {
                    "Healing" => commands.add_component(entity, ProvidesHealing { amount: *n }),
                    "DungeonMap" => commands.add_component(entity, ProvidesDungeonMap {}),
                    _ => println!("Can't provide {}", effect),
                })
        }

        // Add damage component if present
        if let Some(dmg) = &template.base_damage {
            commands.add_component(entity, Damage(*dmg));
            // If the entity is an item, then it's a weapon
            if template.entity_type == EntityType::Item {
                commands.add_component(entity, Weapon {});
            }
        }
    }

    /// Creates the described entities in a World
    pub fn spawn_entities(
        &self,
        ecs: &mut World,
        rng: &mut RandomNumberGenerator,
        level: usize,
        spawn_points: &[Point],
    ) {
        // Store references to templates
        let mut available_entities = Vec::new();
        self.entities
            .iter()
            .filter(|e| e.levels.contains(&level))
            .for_each(|t| {
                for _ in 0..t.frequency {
                    available_entities.push(t)
                }
            });

        let mut commands = CommandBuffer::new(ecs);
        spawn_points.iter().for_each(|pt| {
            if let Some(entity) = rng.random_slice_entry(&available_entities) {
                self.spawn_entity(pt, entity, &mut commands);
            }
        });

        // Apply commands
        commands.flush(ecs);
    }
}
