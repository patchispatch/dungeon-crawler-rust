pub use crate::prelude::*;
use std::collections::HashSet;

/// Renders an entity with a color and a glyph
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Render {
    pub color: ColorPair,
    pub glyph: FontCharType,
}

/// Represents a Player entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Player {
    pub map_level: u32,
}

/// Represents an Enemy entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Enemy;

/// Add random movement to an entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MovingRandomly;

/// Represents a movement intent message
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WantsToMove {
    pub entity: Entity,
    pub destination: Point,
}

/// Represents the Health of an entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Health {
    pub current: i32,
    pub max: i32,
}

/// Gives a name to an entity
#[derive(Clone, PartialEq)]
pub struct Name(pub String);

/// Represents an attack intent message
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WantsToAttack {
    pub attacker: Entity,
    pub victim: Entity,
}

/// Gives an entity player-chasing behaviorproviding healing
pub struct ChasingPlayer;

/// Represents an Item entity
pub struct Item;

/// Represents an Amulet entity
pub struct AmuletOfYala;

/// Gives an entity a field of view
#[derive(Clone, Debug, PartialEq)]
pub struct FieldOfView {
    pub visible_tiles: HashSet<Point>,
    pub radius: i32,
    pub is_dirty: bool,
}

impl FieldOfView {
    pub fn new(radius: i32) -> Self {
        Self {
            visible_tiles: HashSet::new(),
            radius,
            is_dirty: true,
        }
    }

    pub fn clone_dirty(&self) -> Self {
        Self {
            visible_tiles: HashSet::new(),
            radius: self.radius,
            is_dirty: true,
        }
    }
}

/// Give an entity the capability of providing healing once consumed
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ProvidesHealing {
    pub amount: i32,
}

/// Give an entity the capability of showing the map once consumed
pub struct ProvidesDungeonMap;

/// Give an entity the ability to be carried by another
#[derive(Clone, PartialEq)]
pub struct Carried(pub Entity);

/// Represents an item activation intent message
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct ActivateItem {
    pub used_by: Entity,
    pub item: Entity,
}

/// Represents the damage an entity inflicts
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Damage(pub i32);

/// Represents a weapon entity
pub struct Weapon;
