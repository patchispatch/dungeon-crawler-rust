# Short design document template

## Name
Rusty Roguelike

## Short description
Simple dungeon crawler following the Hands-on Rust book, with procedurally generated levels, monsters of increasing difficulty and turn-based movement.

## Story
Monsters attack the hero's hometown, and needs the *Amulet of Yala* to defeat them.

## Basic Game Loops
1. Enter dungeon level
2. Explore, revealing the map
3. Encounter enemies whom the player fights or flees from
4. Find power-ups and use them to strengthen the player
5. Locate the exit to the level -> Go to Step 1

## Minimum Viable Product
- [ ] Create a basic dungeon map
- [ ] Place the player and let them walk around
- [ ] Spawn monsters, draw them, and let the player kill them by walking into them
- [ ] Add health and a combat system that uses it
- [ ] Add healing potions
- [ ] Display a *Game Over* screen when the player dies
- [ ] Add the *Amulet of Yala* to the level and let the player win by reaching it

## Stretch Goals
- [ ] Add Fields of View
- [ ] Add more interesting dungeon designs
- [ ] Add some dungeon themes
- [ ] Add multiple layers to the dungeon, with the Amulet on the last one
- [ ] Add varied weapons to the game
- [ ] Move to a data-driven design for spawning enemies
- [ ] Consider some visual effects to make combat more visceral
- [ ] Consider keeping score




